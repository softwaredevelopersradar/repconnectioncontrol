﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;
namespace WPFControlConnection
{
    /// <summary>
    /// Определяет объект, используемый для создания контрола подключения.
    /// </summary>
    public partial class ConnectionControl : UserControl
    {
        #region Variables
        private static ushort intervalFlash = 300;
        private System.Threading.Timer timerWrite;
        private System.Threading.Timer timerRead;
        #endregion

        #region DependencyProperty
        public static DependencyProperty LabelTextProperty = DependencyProperty.Register(nameof(LabelText), typeof(string), typeof(ConnectionControl), new PropertyMetadata(string.Empty, new PropertyChangedCallback(LabelTextChanged)));

        public static DependencyProperty ButServerHintProperty = DependencyProperty.Register(nameof(ButServerHint), typeof(string), typeof(ConnectionControl), new PropertyMetadata(string.Empty,
            new PropertyChangedCallback(ButServerHintChanged)));

        public static DependencyProperty LEDClientHintProperty = DependencyProperty.Register(nameof(LEDClientHint), typeof(string), typeof(ConnectionControl), new PropertyMetadata(string.Empty,
            new PropertyChangedCallback(LEDClientHintChanged)));

        public static DependencyProperty LabelHintProperty = DependencyProperty.Register(nameof(LabelHint), typeof(string), typeof(ConnectionControl), new PropertyMetadata(string.Empty,
            new PropertyChangedCallback(LabelHintChanged)));

        public static DependencyProperty LedReadDataHintProperty = DependencyProperty.Register(nameof(LedReadDataHint), typeof(string), typeof(ConnectionControl), new PropertyMetadata(string.Empty, new PropertyChangedCallback(LedReadDataHintChanged)));

        public static DependencyProperty LedWriteDataHintProperty = DependencyProperty.Register(nameof(LedWriteDataHint), typeof(string), typeof(ConnectionControl), new PropertyMetadata(string.Empty, new PropertyChangedCallback(LedWriteDataHintChanged)));

        public static DependencyProperty ClientConnectionStateProperty = DependencyProperty.Register(nameof(ClientConnectionState), typeof(ConnectionStates), typeof(ConnectionControl), new PropertyMetadata(ConnectionStates.Unknown, new PropertyChangedCallback(ClientConnectionStateChanged)));

        public static DependencyProperty ServerConnectionStateProperty = DependencyProperty.Register(nameof(ServerConnectionState), typeof(ConnectionStates), typeof(ConnectionControl), new PropertyMetadata(ConnectionStates.Unknown, new PropertyChangedCallback(ServerConnectionStateChanged)));
        #endregion

        #region PropertyChangedCallback

        static void LabelTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ConnectionControl control = (ConnectionControl)d;
            control.LabType.Content = e.NewValue;
        }

        static void ButServerHintChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ConnectionControl control = (ConnectionControl)d;
            control.ButServer.ToolTip = e.NewValue;
        }

        static void LEDClientHintChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ConnectionControl control = (ConnectionControl)d;
            control.RectClient.ToolTip = e.NewValue;
        }

        static void LabelHintChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ConnectionControl control = (ConnectionControl)d;
            control.LabType.ToolTip = e.NewValue;
        }

        static void LedReadDataHintChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ConnectionControl control = (ConnectionControl)d;
            control.EllipseRead.ToolTip = e.NewValue;
        }

        static void LedWriteDataHintChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ConnectionControl control = (ConnectionControl)d;
            control.EllipseWrite.ToolTip = e.NewValue;
        }

        static void ClientConnectionStateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ConnectionControl control = (ConnectionControl)d;

            control.ShowClientConnectionState((ConnectionStates)e.NewValue);
        }

        static void ServerConnectionStateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ConnectionControl control = (ConnectionControl)d;

            control.ShowServerConnectionState((ConnectionStates)e.NewValue);
        }
        #endregion

        #region Properties

        #region Text

        /// <summary>
        /// Получает или задает подпись
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — LB.
        /// </summary>
        public string LabelText
        {
            get { return (string)GetValue(LabelTextProperty); }//{ return LabType.Content.ToString();}
            set { SetValue(LabelTextProperty, value); }
        }

        /// <summary>
        /// Получает или задает текст подсказки для кнопки Сервера
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — ButtonServer. 
        /// </summary>  
        public string ButServerHint
        {
            get { return (string)GetValue(ButServerHintProperty); }//{ return ButServer.ToolTip.ToString(); }
            set { SetValue(ButServerHintProperty, value); }
        }

        /// <summary>
        /// Получает или задает текст подсказки для индикатора подключения Клиента
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — LEDClient.
        /// </summary> 
        public string LEDClientHint
        {
            get { return (string)GetValue(LEDClientHintProperty); }//{ return RectClient.ToolTip.ToString(); }
            set { SetValue(LEDClientHintProperty, value); }
        }

        /// <summary>
        /// Получает или задает текст подсказки для подписи типа оборудования
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — LB. 
        /// </summary>
        public String LabelHint
        {
            get { return (string)GetValue(LabelHintProperty); }//{ return LabType.ToolTip.ToString(); }
            set { SetValue(LabelHintProperty, value); }
        }

        /// <summary>
        /// Получает или задает текст подсказки для индикатора Чтения
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — LEDRead. 
        /// </summary>
        public String LedReadDataHint
        {
            get { return (string)GetValue(LedReadDataHintProperty); }//{ return EllipseRead.ToolTip.ToString(); }
            set { SetValue(LedReadDataHintProperty, value); }
        }

        /// <summary>
        /// Получает или задает текст подсказки для индикатора Записи
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — LEDWrite. 
        /// </summary>
        public String LedWriteDataHint
        {
            get { return (string)GetValue(LedWriteDataHintProperty); }//{ return EllipseWrite.ToolTip.ToString(); }
            set { SetValue(LedWriteDataHintProperty, value); }
        }
        #endregion

        public ConnectionStates ClientConnectionState
        {
            get { return (ConnectionStates)GetValue(ClientConnectionStateProperty); }
            set { SetValue(ClientConnectionStateProperty, value); }
        }

        public ConnectionStates ServerConnectionState
        {
            get { return (ConnectionStates)GetValue(ServerConnectionStateProperty); }
            set { SetValue(ServerConnectionStateProperty, value); }
        }

        #region Visibility

        /// <summary>
        /// Получает или задает значение, указывающее, отображаются ли Label
        /// Значение true, если Label отображается; в противном случае — значение false. Значение по умолчанию
        /// </summary>        
        public bool LabelVisible
        {
            get
            {
                if (LabType.Visibility != Visibility.Visible)
                    return false;
                else
                    return true;
            }
            set
            {
                if (value == true)
                    LabType.Visibility = Visibility.Visible;
                else
                    LabType.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Получает или задает значение, указывающее, отображаются ли индикаторы отпрваки и получения данных
        /// Возвращает:
        /// Значение true, если индикаторы
        /// отображается; в противном случае — значение false. Значение по умолчанию — true. 
        /// </summary>
        public bool LedTransVisible
        {
            get
            {
                if (EllipseWrite.Visibility != Visibility.Visible)
                    return false;
                else
                    return true;
            }
            set
            {
                if (value == true)
                {
                    EllipseWrite.Visibility = Visibility.Visible;
                    EllipseRead.Visibility = Visibility.Visible;
                }
                else
                {
                    EllipseWrite.Visibility = Visibility.Collapsed;
                    EllipseRead.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        ///  Получает или задает значение, указывающее, отображается индикатора подключения Клиента
        ///  Возвращает:
        ///     Значение true, если индикатор
        ///     отображается; в противном случае — значение false. Значение по умолчанию — true. 
        /// </summary>        
        public bool LEDClientVisible
        {
            get
            {
                if (RectClient.Visibility != Visibility.Visible)
                    return false;
                else
                    return true;
            }
            set
            {
                if (value == true)
                    RectClient.Visibility = Visibility.Visible;
                else
                    RectClient.Visibility = Visibility.Collapsed;
            }
        }

        #endregion

        #region Color

        /// <summary>
        /// Получает или задает цвет кнопки отвечающей за подкключение/создание Сервера
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — Brashes.LyghtGray. 
        /// </summary>
        public Brush ButServerColor
        {
            get { return ButServer.Background; }
            set { ButServer.Background = value; }
        }

        /// <summary>
        ///  Получает или задает цвет индикатора подключения Клиента
        ///  Возвращает:
        ///  Объект с содержимым элемента управления. Значение по умолчанию — Brashes.LyghtGray.
        /// </summary>
        public Brush LEDClientColor
        {
            get { return RectClient.Fill; }
            set { RectClient.Fill = value; }
        }

        /// <summary>
        /// Получает или задает цвет индикатора Чтения
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — Brashes.LyghtGray.
        /// </summary>
        public RadialGradientBrush ReadDataColor
        {
            get;
            set;
        } = new RadialGradientBrush(new GradientStopCollection(new List<GradientStop>()
        { new GradientStop(Color.FromRgb(103,0,0), 1), new GradientStop(Color.FromRgb(255, 0, 0), 0.316) }));

        /// <summary>
        /// Получает или задает цвет индикатора Записи
        /// Возвращает:
        ///     Объект с содержимым элемента управления. Значение по умолчанию — Brashes.LyghtGray.
        /// </summary>
        public RadialGradientBrush WriteDataColor { get; set; } = new RadialGradientBrush(new GradientStopCollection(new List<GradientStop>()
        { new GradientStop(Color.FromRgb(0,103,0), 1), new GradientStop(Color.FromRgb(0, 255, 0), 0.316) }));

        /// <summary>
        /// Получает или задает цвет Подключения
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — Brushes.Green. 
        /// </summary>
        public LinearGradientBrush ColorConnect = new LinearGradientBrush(new GradientStopCollection(new List<GradientStop>()
        { new GradientStop(Color.FromRgb(15, 91, 19), 1),  new GradientStop(Color.FromRgb(0, 255, 31), 0.002)}), new Point(0.5, 0), new Point(0.5, 1));

        /// <summary>
        /// Получает или задает цвет Отсоединения
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — Brushes.Red. 
        /// </summary>
        public LinearGradientBrush ColorDisconnect = new LinearGradientBrush(new GradientStopCollection(new List<GradientStop>()
        { new GradientStop(Color.FromRgb(66, 3, 3), 1),  new GradientStop(Color.FromRgb(255, 0, 0), 0.002)}), new Point(0.5, 0), new Point(0.5, 1));

        /// <summary>
        /// Получает или задает цвет Неопределенного состояния
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — Brushes.LightGray.
        /// </summary>  
        public RadialGradientBrush ColorLedNone { get; set; } = new RadialGradientBrush(new GradientStopCollection(new List<GradientStop>()
        { new GradientStop(Color.FromRgb(104, 104, 104), 1),  new GradientStop(Color.FromRgb(255, 255, 255), 0.141)}));

        /// <summary>
        /// Получает или задает цвет Неопределенного состояния
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — Brushes.LightGray.
        /// </summary>  
        public LinearGradientBrush ColorButNone { get; set; } = new LinearGradientBrush(new GradientStopCollection(new List<GradientStop>()
        { new GradientStop(Color.FromRgb(0, 0, 0), 1),  new GradientStop(Color.FromRgb(104, 104, 104), 0.013)}), new Point(0.5, 0), new Point(0.5, 1));

        #endregion

        /// <summary>
        /// Получает или задает интервал индикации (запись/чтение)
        /// Возвращает:
        /// Объект с содержимым элемента управления. Значение по умолчанию — 300 мс. 
        /// </summary>
        public ushort IntervalFlash
        {
            get { return intervalFlash; }
            set { intervalFlash = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// Индикация чтения
        /// </summary>
        public void ShowRead()
        {
            EllipseRead.Fill = ReadDataColor;
            timerRead = new System.Threading.Timer(TimerCallBack, EllipseRead, intervalFlash, 0);
        }

        /// <summary>
        /// Индикация записи
        /// </summary>
        public void ShowWrite()
        {
            EllipseWrite.Fill = WriteDataColor;
            timerWrite = new System.Threading.Timer(TimerCallBackWrite, EllipseWrite, intervalFlash, 0);
        }

        private void TimerCallBack(object obj)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new delShowChange(() =>
            {
                (obj as Ellipse).Fill = ColorLedNone;
                timerRead.Dispose();
            }));
        }

        private void TimerCallBackWrite(object obj)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new delShowChange(() =>
            {
                (obj as Ellipse).Fill = ColorLedNone;
                timerWrite.Dispose();
            }));
        }

        public void ShowUknownStateClient() { ClientConnectionState = ConnectionStates.Unknown; }

        /// <summary>
        /// Индикация поключения клиента
        /// </summary>
        public void ShowConnectClient() { ClientConnectionState = ConnectionStates.Connected; }

        /// <summary>
        /// Индикация отсоединения клиента
        /// </summary>
        public void ShowDisconnectClient() { ClientConnectionState = ConnectionStates.Disconnected; }

        public void ShowUnkownStateServer() { ServerConnectionState = ConnectionStates.Unknown; }

        /// <summary>
        /// Индикация создания Сервера/ подключения к нему
        /// </summary>
        public void ShowConnect() { ServerConnectionState = ConnectionStates.Connected; }

        /// <summary>
        /// Индикация отсоединения от сервера/ разрушение сервера
        /// </summary>
        public void ShowDisconnect() { ServerConnectionState = ConnectionStates.Disconnected; }

        private void ButServer_Click(object sender, RoutedEventArgs e)
        {
            ButServerClick(sender, e);
        }

        private void ShowClientConnectionState(ConnectionStates connectionState)
        {
            switch (connectionState)
            {
                case ConnectionStates.Unknown:
                    LEDClientColor = ColorLedNone;
                    break;
                case ConnectionStates.Connected:
                    LEDClientColor = ColorConnect;
                    break;
                case ConnectionStates.Disconnected:
                    LEDClientColor = ColorDisconnect;
                    break;
            }

        }

        private void ShowServerConnectionState(ConnectionStates connectionState)
        {
            switch (connectionState)
            {
                case ConnectionStates.Unknown:
                    ButServerColor = ColorButNone;
                    break;
                case ConnectionStates.Connected:
                    ButServerColor = ColorConnect;
                    break;
                case ConnectionStates.Disconnected:
                    ButServerColor = ColorDisconnect;
                    break;
            }

        }

        /// <summary>
        /// Инициализация компонентов
        /// </summary>
        public ConnectionControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Events

        /// <summary>
        /// Событие срабатывает по нажатию на кнопку сервера
        /// </summary>
        public event RoutedEventHandler ButServerClick = (object sender, RoutedEventArgs e) => { };
        #endregion

        #region Delegate
        private delegate void delShowChange();
        #endregion
    }
}
