﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFControlConnection
{
    public enum ConnectionStates:byte
    {
        Connected,
        Disconnected,
        Unknown
    }
}
