﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFControlConnection;

namespace WpfTest
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = null)
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }
        bool flag = true;
        string lableText = "TEST";
        public string LabelText
        {
            get => lableText;
            set
            {
                if (lableText == value) return;
                lableText = value;
                OnPropertyChanged();
            }
        } 
        private void tryConnect_Loaded(object sender, RoutedEventArgs e)
        {
            if (flag)
            {
                tryConnect.ShowConnect();
                tryConnect.ShowDisconnectClient();
                tryConnect.ShowRead();
                flag = false;
            }
            else
            {
                tryConnect.ShowDisconnect();
                tryConnect.ShowConnectClient();
                tryConnect.ShowWrite();
                flag = true;
            }
        }

        private void DataBaseConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            LabelText = "123";

        }

        private ConnectionStates _testState;
        public ConnectionStates TestState
        {
            get => _testState;
            set
            {
                _testState = value;
                OnPropertyChanged(nameof(TestState));
            }
        }

        private ConnectionStates _testStateClient;
        public ConnectionStates TestStateClient
        {
            get => _testStateClient;
            set
            {
                _testStateClient = value;
                OnPropertyChanged(nameof(TestStateClient));
            }
        }

        Random rand = new Random();
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var codeState = (byte)rand.Next(0, 2);

            TestState = (ConnectionStates)codeState;

            TestStateClient = (ConnectionStates)((byte)rand.Next(0, 2));
        }
    }
}
